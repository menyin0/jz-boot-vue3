import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '手机号（用于和sys_user关联）',
    align: "center",
    dataIndex: 'phoneNumber'
  },
  {
    title: '简历内容（暂时精简后续扩展）',
    align: "center",
    dataIndex: 'resumeContent'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '手机号（用于和sys_user关联）',
    field: 'phoneNumber',
    component: 'Input',
  },
  {
    label: '简历内容（暂时精简后续扩展）',
    field: 'resumeContent',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];

// 高级查询数据
export const superQuerySchema = {
  phoneNumber: {title: '手机号（用于和sys_user关联）',order: 0,view: 'text', type: 'string',},
  resumeContent: {title: '简历内容（暂时精简后续扩展）',order: 1,view: 'text', type: 'string',},
};
